package org.example.repo;


import org.example.es.StudentEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepo extends ElasticsearchRepository<StudentEntity,String> {
    List<StudentEntity> findByFirstNameAndLastName(String firstName, String lastName);
}
