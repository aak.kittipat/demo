package org.example.controller;

import org.example.StudentStatus;
import org.example.domain.StudentCriteria;
import org.example.es.StudentEntity;
import org.example.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
public class Controller {

    @Autowired
    StudentService service;

    @PostMapping("/getStudentCriteria")
    public List<StudentEntity> getStudentCriteria(@RequestBody StudentCriteria criteria,@RequestBody String status){
        return service.getStudentCriteria(criteria);
    }

    @PostMapping("/getDynamicStudent")
    public List<StudentEntity> getDynamicStudent(@RequestParam("firstName") String firstName,@RequestParam("age") Integer age){
        return service.getDynamicStudent(firstName, age);
    }

    @GetMapping("/getDynamicStudent")
    public List<StudentEntity> test(String firstName, String lastName) {
        return service.test(firstName, lastName);
    }

}
