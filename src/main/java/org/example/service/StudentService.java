package org.example.service;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.example.domain.StudentCriteria;
import org.example.es.StudentEntity;
import org.example.repo.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHitsIterator;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentService {

    @Autowired
    private StudentRepo repo;

    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    @Autowired
    public void init() {
        List<StudentEntity> entities = new ArrayList<>();
        repo.deleteAll();
        StudentEntity entity = new StudentEntity();
        entity.setFirstName("StudentA");
        entity.setLastName("lNameA");
        entity.setGrade("B");
        entity.setDepartment("Computer engineer");
        entity.setFaculty("Engineer");
        entity.setGender("male");
        entities.add(entity);

        entity = new StudentEntity();
        entity.setFirstName("StudentB");
        entity.setLastName("lNameA");
        entity.setGrade("A");
        entity.setDepartment("Doctor");
        entity.setFaculty("Doctor");
        entity.setGender("female");
        entity.setAge(22);
        entities.add(entity);

        entity = new StudentEntity();
        entity.setFirstName("StudentC");
        entity.setLastName("lNameC");
        entity.setGrade("B");
        entity.setDepartment("Computer engineer");
        entity.setFaculty("Engineer");
        entity.setGender("male");
        entity.setAge(21);
        entities.add(entity);

        entity = new StudentEntity();
        entity.setFirstName("StudentD");
        entity.setLastName("lNameC");
        entity.setGrade("D+");
        entity.setDepartment("Computer engineer");
        entity.setFaculty("Engineer");
        entity.setGender("female");
        entity.setAge(20);
        entities.add(entity);

        entity = new StudentEntity();
        entity.setFirstName("StudentE");
        entity.setLastName("lNameE");
        entity.setGrade("C");
        entity.setDepartment("Chemical engineer");
        entity.setFaculty("Engineer");
        entity.setGender("male");
        entity.setAge(19);
        entities.add(entity);
        repo.saveAll(entities);
    }

    public List<StudentEntity> test(String firstName, String lastName) {
        return repo.findByFirstNameAndLastName(firstName, lastName);
    }


    public List<StudentEntity> getStudentCriteria(StudentCriteria criteria){
        List<StudentEntity> entities = new ArrayList<>();
        BoolQueryBuilder qb = QueryBuilders.boolQuery();
        if(criteria.getFirstName() != null) {
            qb.must(QueryBuilders.matchQuery("firstName", criteria.getFirstName()));
        }
        if(criteria.getLastName() != null){
            qb.must(QueryBuilders.matchQuery("lastName", criteria.getLastName()));
        }
        if(criteria.getFaculty() != null){
            qb.must(QueryBuilders.matchQuery("faculty", criteria.getFaculty()));
        }
        if(criteria.getDepartment() != null) {
            qb.must(QueryBuilders.matchQuery( "department",criteria.getDepartment()));
        }
        if(criteria.getGender() != null) {
            qb.must(QueryBuilders.matchQuery("gender", criteria.getGender()));
        }
        if(criteria.getGrade() != null) {
            qb.must(QueryBuilders.matchQuery("grade", criteria.getGrade()));
        }
        if(criteria.getAge() != null) {
            qb.must(QueryBuilders.matchQuery("age", criteria.getAge()));
        }
        if(criteria.getBod() != null) {
            qb.must(QueryBuilders.matchQuery("grade", criteria.getBod()));
        }
        NativeSearchQueryBuilder searchQuery = new NativeSearchQueryBuilder().withQuery(qb);
        SearchHitsIterator<StudentEntity> hits = elasticsearchRestTemplate.searchForStream(searchQuery.build(),
                StudentEntity.class);
        hits.forEachRemaining(hit -> entities.add(hit.getContent()));
        return entities;
    }

    public List<StudentEntity> getDynamicStudent(String firstName, Integer age){
        BoolQueryBuilder mainQuery = QueryBuilders.boolQuery();
        mainQuery.must(QueryBuilders.wildcardQuery("firstName", firstName));
        mainQuery.must(QueryBuilders.rangeQuery("age").gt(age));
        NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder().withQuery(mainQuery);
        SearchHitsIterator<StudentEntity> hits = elasticsearchRestTemplate.searchForStream(nativeSearchQueryBuilder.build(),StudentEntity.class);
        List<StudentEntity> entityList = new ArrayList<>();
        hits.forEachRemaining(hit -> entityList.add(hit.getContent()));
        return entityList;
    }
}
